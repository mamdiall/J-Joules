/**
 * 
 */
package org.powerapi.jjoules.energy.device;

/**
 * @author sanoussy
 *
 */
public class DeviceNotConfiguredException extends JJoulesException {

	/**
	 * 
	 */
	public DeviceNotConfiguredException() {
	}

	/**
	 * @param message
	 */
	public DeviceNotConfiguredException(String message) {
		super(message);
	}
}
