/**
 * 
 */
package org.powerapi.jjoules.energy.device;

/**
 * @author sanoussy
 *
 */
public class JJoulesException extends Exception {

	/**
	 * 
	 */
	public JJoulesException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public JJoulesException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	public JJoulesException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public JJoulesException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
